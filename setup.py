from setuptools import setup, find_packages
import os

version = '0.1'

setup(name='fedoratheme.kindofblue',
      version=version,
      description="An installable theme for Plone 3.0",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='web zope plone theme fedora kindofblue',
      author='Hrishikesh Ballal, Diana Fong, Mohd Izhar Firdaus',
      author_email='webmaster@fedoraproject.org',
      url='http://fedoraproject.org/',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['fedoratheme'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
