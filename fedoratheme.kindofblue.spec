# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%define python /usr/bin/python2.4
%define zopehome /usr/lib/zope
%{!?python_sitelib: %define python_sitelib %(%{python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           fedoratheme.kindofblue
Version:        0.1
Release:        1%{?dist}
Summary:        a port of Fedora wiki's kindofblue theme to plone

Group:          Development/Languages
License:        GPLv2+
URL:            http://fedora.foss.org.my
Source0:        %{name}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  compat-python24-devel

%description
A port of Fedora wiki's kindofblue theme to plone.

%prep
%setup -q -n fedoratheme.kindofblue


%build
# Remove CFLAGS=... for noarch packages (unneeded)
CFLAGS="$RPM_OPT_FLAGS" %{python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{python} setup.py install --home=%{zopehome} -O1 --skip-build --root $RPM_BUILD_ROOT 

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
# For noarch packages: sitelib
%{zopehome}/lib/python/


%changelog
